<?php

namespace Siteset\Sitemap;

use Illuminate\Support\ServiceProvider;

class SitemapServiceProvider extends ServiceProvider
{
	/**
	 *
	 * @return void
	 */

	public function register() //: void
	{
		$this->publishes([
			__DIR__ . '/../config/sitemap.php' => config_path('sitemap.php')
		], 'config');

		$this->loadViewsFrom(__DIR__ . '/../views/', 'sitemap');
	}

	/**
	 *
	 * @return void
	 */

	public function boot() //: void
	{
		//
	}
}
