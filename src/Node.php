<?php

namespace Siteset\Sitemap;

class Node
{
	/**
	 *
	 * @var array
	 */

	private $property = [];

	/**
	 *
	 *
	 */

	public function __construct($record = null)
	{
		$this->record = $record;
	}

	/**
	 * метод имитирует запись значения свойств
	 * - name		- имя страницы для html версии
	 * - loc		- урл
	 * - lastmod	- последнее изменение страницы
	 * - changefreq	- как часто меняется страница (always, hourly, daily, weekly,
	 * 					monthly, yearly, never)
	 * - priority	- приоритет индексирования (от 0 до 100)
	 * - fake		- является ли узел фейковым. (при значение true отключает
	 * 					попытку запустить метод обработчик для узла)
	 * - pagination	- явялется ли узел постраничкой (при значение true отключает
	 * 					попытку запустить метод обработчик для узла)
	 * - record		- данные на основе которых был создан узел
	 * - nodes		- список дочерних узлов
	 * - root		- рутовый узел
	 *
	 * @param string $name имя вызываемого свойства
	 * @param array $argument аргументы
	 */

	public function __set($name, $argument)
	{
		// сохраняем данные
		$this->property[$name] = $argument;
	}

	/**
	 * метод имитирует вызов свойства
	 *
	 * @return mixed
	 *
	 * @param string $name имя вызываемого свойства
	 */

	public function __get($name)
	{
		switch ($name) {
			case 'lastmod':
				return $this->property[$name] ?? config('sitemap.lastmod');
				break;

			case 'changefreq':
				return $this->property[$name] ?? config('sitemap.changefreq');
				break;

			case 'priority':
				return $this->property[$name] ?? config('sitemap.priority');
				break;

			case 'fake':
			case 'pagination':
			case 'root':
				return $this->property[$name] ?? false;
				break;

			case 'nodes':
				return $this->property[$name] ?? collect();
				break;

			default:
				return $this->property[$name] ?? null;
				break;
		}
	}
}
