<?php

namespace Siteset\Sitemap;

use Route;
use Response;
use Illuminate\Support\Facades\Cache;

abstract class Handler
{
	/**
	 * древовидный список роутов
	 *
	 * @var \Siteset\Sitemap\Map
	 */

	private $map;

	/**
	 * список доп роутов для создания псевдо-дерева
	 * например что бы страницы
	 * /news/group1
	 * /news/article1
	 * /news/group2
	 * /news/article2
	 * /news/article3
	 *
	 * выводились в карте вот так
	 * /news/group1
	 * 		/news/article1
	 * /news/group2
	 * 		/news/article2
	 * 		/news/article3
	 *
	 * @var array
	 */

	protected $additionalRoutes = [
		//'фейковое имя роута' => 'фейковый урл для дерева'
	];

	/**
	 * принудительная сортировка роутов
	 *
	 * @var array
	 */

	protected $position = [
		// 'имя роута'
	];

	/**
	 * генератор карты
	 *
	 * @param Closure|null $filter
	 */

	public function generate($filter = null)
	{
		if (null === (
			$this->map = Cache::get(config('sitemap.cache.name') . '.' . parse_url(url()->current(), PHP_URL_HOST))
		)) {
			$routes = collect(Route::getRoutes()->getRoutesByName())->transform(function ($route) {
				return $route->uri;
			});

			// дополняем коллекцию роутов
			foreach ($this->additionalRoutes as $name => $route)
				$routes->put($name, $route);

			// позиционирование
			foreach (array_reverse($this->position) as $route) {
				// получаем uri
				$uri = $routes->get($route);
				// удаляем роут
				unset($routes[$route]);

				// переносим роут вверх
				$routes->prepend($uri, $route);
			}

			// если есть фильтрация
			if (!is_null($filter))
				// запускаем ее
				call_user_func($filter, $routes);

			$this->collect($routes);

			// листаем рекурсивно узлы и
			// вызываем методы обработчики
			$this->run($this->map);

			Cache::put(
				config('sitemap.cache.name') . '.' . parse_url(url()->current(), PHP_URL_HOST),
				$this->map,
				now()->addMinutes(config('sitemap.cache.minutes'))
			);
		}
	}

	/**
	 * формирования xml или html карты
	 *
	 * @param string|null $view
	 */

	public function render($view = null)
	{
		if (null === $view)
			$view = config('sitemap.view');

		switch ($view) {
			case 'xml':
				$dom = new \DOMDocument('1.0', 'UTF-8');
				$dom->formatOutput = true;

				$root     = $dom->createElement('urlset');
				$rootAttr = $dom->createAttribute('xmlns');
				$rootAttr->value = 'http://www.sitemaps.org/schemas/sitemap/0.9';
				$root->appendChild($rootAttr);

				$this->parse($this->map->nodes, $dom, $root);

				$dom->appendChild($root);

				return Response::make(
					$dom->saveXML(),
					200,
					['Content-type' => 'text/xml; charset=utf-8']
				);
				break;

			default:
				return view($view, [
					'sitemap' => call_user_func(function () {
						$sitemap		= new Node();
						$sitemap->nodes	= $this->map->nodes;
						$sitemap->root	= true;

						$node			= $sitemap->nodes->first();
						$node->root		= true;

						return $sitemap;
					}),
				])->render();
				break;
		}
	}

	/**
	 * проход по роутам + запуск методов
	 * обработчиков для каждого
	 *
	 * @param \Siteset\Sitemap\Map $map объект карты
	 */

	private function run($map)
	{
		$method = camel_case(str_replace('.', '_', $map->name));

		// проверяем есть ли метод обработчик для роута
		if (true === method_exists(static::class, $method)) {

			// если рутовый узел
			if (null === $map->parent)
				static::$method($map->nodes);

			// если вложенные роуты
			elseif ($map->parent->nodes->count())
				// листаем узлы родительского роута
				foreach ($map->parent->nodes as $node) {
					// если узел фейк
					if (true === $node->fake)
						continue;

					// если узел постраничка
					if (true === $node->pagination)
						continue;

					// определяем сколько аргументов принимает метод
					$reflection = new \ReflectionMethod(static::class, $method);
					// если одни
					if (1 === count($reflection->getParameters()))
						// запускаем метод обработчик для роута с одинм аргументом
						static::$method($nodes = collect());

					// если более 1-ого
					else
						// запускаем метод обработчик для с двумя (если их более чем
						// два то вылетит исключение) аргументами
						static::$method($nodes = collect(), $node->record);

					// собираем полученые узлы
					$node->nodes = $node->nodes ?? collect();
					$nodes->each(function ($record) use ($map, $node) {
						$map	->nodes->push($record);
						$node	->nodes->push($record);
					});
				}

			// запускаем дочерние роуты
			$map->maps->each(function ($map) {
				$this->run($map);
			});
		}
	}

	/**
	 * формирование древовидной карты роутов
	 *
	 * @param collect $routes список роутов
	 * @param \Siteset\Sitemap\Map|null $map объект карты
	 * @param integer $level уровень урла
	 */

	private function collect($routes, $map = null, $level = 0)
	{
		// рутовый роут
		if (null === $map) {
			$index = $routes->filter(function ($uri) {
				return $uri === '/';
			});

			$this->map = new Map($index->keys()[0], $index->first());
			$this->collect($routes, $this->map, $level + 1);
		}

		// оставльные роуты
		else {
			$filtered = $routes->filter(function ($uri) use ($level) {
				$nodes	= collect(explode('/', trim($uri, '/')));

				return $nodes->count() === $level;
			});

			$filtered->each(function ($uri, $name) use ($level, $map, $routes) {
				// если первый уровень
				if (1 === $level) {
					// переносим в карту все что НЕ "/"
					if ('/' !== $uri)
						$this->collect($routes, $map->add($name, $uri, $map), $level + 1);
				}

				// если последующие роуты
				else {
					// переносим в карту только те роуты которые
					// начинаются на $map->uri . '/'
					if (starts_with($uri, $map->uri . '/'))
						$this->collect($routes, $map->add($name, $uri, $map), $level + 1);
				}
			});
		}
	}

	/**
	 * формирование xml из узлов
	 *
	 * @param collect $nodes
	 * @param \DOMDocument $dom
	 * @param \DOMDocument $root
	 * @param collect $saved
	 */

	private function parse($nodes, $dom, $root, $saved = null)
	{
		$saved = $saved ?? collect();

		if (null !== $nodes && $nodes->count())
			$nodes->each(function ($node) use ($dom, $root, $saved) {
				// если урл уже есть в доме
				if (null !== $saved->get($node->loc))
					// выходим из цикла
					return;

				// если урла еще нет в доме
				else {
					// заполняем элемент url
					$url		= $dom->createElement('url');
					$loc		= $dom->createElement('loc',		$node->loc);
					$lastmod	= $dom->createElement('lastmod',	$node->lastmod->format('Y-m-d'));
					$changefreq	= $dom->createElement('changefreq',	$node->changefreq);
					$priority	= $dom->createElement('priority',	number_format($node->priority / 100, 1, '.', ''));

					$url->appendChild($loc);
					$url->appendChild($lastmod);
					$url->appendChild($changefreq);
					$url->appendChild($priority);

					// сохраняем в доме
					$root->appendChild($url);

					// фиксируем урл как сохраненный
					$saved->put($node->loc, true);

					// рекурсивный парсер
					$this->parse($node->nodes, $dom, $root, $saved);
				}
			});
	}
}
