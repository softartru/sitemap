<?php

namespace Siteset\Sitemap;

class Map
{
	/**
	 *
	 * @var array
	 */

	private $property = [];

	/**
	 *
	 * @param string $name
	 * @param string $uri
	 * @param \Siteset\Sitemap\Map|null $parent
	 */

	public function __construct($name, $uri, $parent = null)
	{
		$this->parent	= $parent;
		$this->name		= $name;
		$this->uri		= $uri;
		$this->maps		= collect();
		$this->nodes	= collect();
	}

	/**
	 * метод имитирует запись значения свойств
	 * - name	- string
	 * - uri	- string
	 * - parent	- \Siteset\Sitemap\Map или null
	 * - maps	- collect
	 * - nodes	- collect
	 *
	 * @param string $name имя вызываемого свойства
	 * @param array $argument аргументы
	 */

	public function __set($name, $argument)
	{
		// сохраняем данные
		$this->property[$name] = $argument;
	}

	/**
	 * метод имитирует вызов свойства
	 *
	 * @return mixed
	 *
	 * @param string $name имя вызываемого свойства
	 */

	public function __get($name)
	{
		return $this->property[$name] ?? null;
	}

	/**
	 *
	 * @return \Siteset\Sitemap\Map
	 *
	 * @param string $name
	 * @param string $uri
	 * @param \Siteset\Sitemap\Map|null $parent
	 */

	public function add($name, $uri, $parent)
	{
		$this->maps->push(
			$map = new Map($name, $uri, $parent)
		);

		return $map;
	}
}
