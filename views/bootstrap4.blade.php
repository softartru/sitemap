<ul class="list-unstyled{{ false === $sitemap->root ? ' pl-3' : '' }}">
	@foreach ($sitemap->nodes as $node)
		@if (true === $node->pagination)
			<li class="list-inline-item"><a href="{{ $node->loc }}">{{ $node->name }}</a></li>
		@else
			<li>
				<a href="{{ $node->loc }}">{{ $node->name }}</a>

				@if (null !== $node->nodes && $node->nodes->count())
					@include('sitemap::bootstrap4', [
						'sitemap' => $node,
					])
				@endif
			</li>
		@endif
	@endforeach
</ul>